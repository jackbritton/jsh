#include "jsh.h"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <wordexp.h>

#include <stdio.h>

// TODO Replace strtok so that we can have quoted text.

inline char jsh_is_whitespace(char c) {
    return c == ' ' || c == '\t' || c == '\n' || c == '\a' || c == '\r';
}

int jsh_parse(char *out[], size_t out_length, char *in) {

    size_t in_iter;
    size_t in_length;
    size_t out_iter;

    size_t start,
           end;

    char whitespace; // Are we reading whitespace?
    char quotes;     // Are we quoting?

    char c;
    char *word;
    
    whitespace = 1;
    quotes = 0;
    in_length = strlen(in);
    for (in_iter = 0; in_iter < in_length; in_iter++) {
        c = in[in_iter];

        if (c == '\'') { // If we hit a quote, toggle quotes.
            quotes = !quotes;
            continue;
        }

        // If nothing's changed..
        if (whitespace && jsh_is_whitespace(c))
            continue;
        if (!whitespace && !jsh_is_whitespace(c))
            continue;

        // New word.
        if (whitespace && !jsh_is_whitespace(c)) {
            start = in_iter;
            whitespace = 0;
            continue;
        }
        // Ended a word.
        if (!whitespace && jsh_is_whitespace(c)) {
            end = in_iter;
            whitespace = 1;
            // TODO Call wordexp and junk. Wish there was just a length arg in wordexp.
            word = malloc(end-start);
            strncpy(word, &in[start], end-start);
            printf("%s\n", word);
            free(word);
            continue;
        }
    }

    return 0;
}

int jsh_execute(char *argv[]) {

    pid_t child,
          wait;
    int status;

    if (argv[0] != NULL && strcmp(argv[0], "cd") == 0) {
        if (argv[1] != NULL) {
            if (chdir(argv[1]) == -1)
                return 1;
            return 0;
        } else {
            return 2;
        }
    }


    child = fork();

    if (child == -1) {
        return 3; // Not enough memory to fork.
    }

    if (child == 0) {
        execvp(argv[0], argv); // Try to execute the command.
        return 4; // Command not found.
    }

    // Wait.
    do {
        wait = waitpid(child, &status, 0);
        if (wait == -1)
            return 5; // Wait failed.
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));

    return 0;
}
