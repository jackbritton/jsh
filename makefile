CC = gcc
CFLAGS = -c -Wall
LFLAGS =
TARGET = jsh
OBJS = main.o jsh.o
DEPS = jsh.h

$(TARGET) : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(TARGET)

%.o : %.c $(DEPS)
	$(CC) $(CFLAGS) $< -o $@

clean :
	rm $(OBJS) $(TARGET)
