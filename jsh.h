#ifndef JSH_H
#define JSH_H

#include <stddef.h>

int jsh_parse(char *out[], size_t out_length, char *in);
int jsh_execute(char *argv[]);

#endif
