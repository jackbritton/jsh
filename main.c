#include <stdio.h>
#include <libgen.h> // for basename
#include "jsh.h"

// TODO quoted text. Probably will have to replace strtok_r
// TODO better errors but don't over-engineer

#define INPUT_BUFFER_LENGTH 512
#define ARG_BUFFER_LENGTH  128

int main(int argc, char *argv[]) {

    char input_buffer[INPUT_BUFFER_LENGTH];
    char *arg_buffer[ARG_BUFFER_LENGTH];
    int error;

    while (1) {

        printf("jsh> ");
        fflush(stdout);
        if (fgets(input_buffer, INPUT_BUFFER_LENGTH, stdin) == NULL)
            break;

        if (jsh_parse(arg_buffer, ARG_BUFFER_LENGTH, input_buffer) == 1) {
            printf("-%s: %s: ARG_BUFFER_LENGTH was too small\n", basename(argv[0]), arg_buffer[0]);
            continue;
        }

        /*
        error = jsh_execute(arg_buffer);
        if (error == 1)
            printf("-%s: %s: %s: not a valid directory\n", basename(argv[0]), arg_buffer[0], arg_buffer[1]);
        else if (error == 2)
            printf("-%s: %s: usage: cd DIR\n", basename(argv[0]), arg_buffer[0]);
        else if (error == 3)
            printf("-%s: %s: not enough system memory to fork\n", basename(argv[0]), arg_buffer[0]);
        else if (error == 4)
            printf("-%s: %s: command not found\n", basename(argv[0]), arg_buffer[0]);
        else if (error == 5)
            printf("-%s: %s: waitpid failed\n", basename(argv[0]), arg_buffer[0]);
        */
    }
    return 0;
}
